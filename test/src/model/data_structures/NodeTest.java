package model.data_structures;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class NodeTest
{
	// Atributos
	
	private Node<String> node1;
	
	private Node<String> node2;
	
	// Scenario

	@Before
	public void setup()
	{
		node1 = new Node<String>("Hola");
		node2 = new Node<String>("Bye");
	}
	
	// Tests
	
	@Test
	public void test1()
	{
		assertNotNull("El nodo no deber�a ser null.", node1);
		assertNotNull("El nodo no deber�a estar vac�o.", node1.darElemento());
		assertNull("El siguiente del nodo deber�a ser null.", node1.darSiguiente());
		node1.cambiarSiguiente(node2);
		assertNotNull("El siguiente del nodo no deber�a ser null.", node1.darSiguiente());
	}

}
