package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

import api.IMovingViolationsManager;
import model.vo.VOMovingViolations;
import model.data_structures.LinkedList;

public class MovingViolationsManager implements IMovingViolationsManager 
{
	// Atributos
	
	/**
	 * Atributo que representa la lista de informaci�n acerca de las infracciones. 
	 */
	LinkedList<VOMovingViolations> lista;
	
	// Constructor
	
	/**
	 * Construye el manager de las informaci�n acerca de las infracciones.
	 */
	public MovingViolationsManager()
	{
		lista = new LinkedList<VOMovingViolations>();
	}

	// M�todos
	
	/**
	 * M�todo encargado de leer el archivo CSV y pasar toda la informaci�n a una LinkedList. 
	 *<b>Pre:<\b> el archivo debe ser CSV. <br>
	 * @param movingViolationsFile nombre del archivo a leer. 
	 */
	public void loadMovingViolations(String movingViolationsFile)
	{
		try 
		{
			FileReader fr = new FileReader(movingViolationsFile); // Sacado de StackOverflow. 
			CSVReader reader = new CSVReaderBuilder(fr).withSkipLines(1).build();
			List<String[]> info = reader.readAll(); // Primero se debe pasar a una lista debido al m�todo readAll().
			for(String[] i : info) // Se recorre primero cada fila. 
			{
				lista.add(new VOMovingViolations(Integer.parseInt(i[0]), i[2], i[14], i[13], Integer.parseInt(i[9]), i[12], i[15]));
			}
		} 
		catch (Exception e) 
		{
			System.out.println(e.getMessage());
		}
	}

	@Override
	/**
	 * M�todo encargado de devolver la lista de infracciones por c�digo de violaci�n. 
	 */
	public LinkedList<VOMovingViolations> getMovingViolationsByViolationCode (String violationCode) 
	{
		LinkedList<VOMovingViolations> act = new LinkedList<VOMovingViolations>();
		for(VOMovingViolations vio : lista)
		{
			if(vio.getCode().compareTo(violationCode) == 0)
				act.add(vio);
		}
		return act;
	}

	@Override
	/**
	 * M�todo encargado de devolver la lista con las infracciones por indicador de accidente.
	 */
	public LinkedList<VOMovingViolations> getMovingViolationsByAccident(String accidentIndicator) 
	{
		LinkedList<VOMovingViolations> act = new LinkedList<VOMovingViolations>();
		Iterator<VOMovingViolations> it = lista.iterator();
		while(it.hasNext())
		{
			VOMovingViolations a = it.next();
			if(a.getAccidentIndicator().equals(accidentIndicator))
				act.add(a);
		}
		return act;
	}	
}
