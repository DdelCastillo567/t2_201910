package model.vo;

/**
 * Representation of a Trip object
 */
public class VOMovingViolations implements Comparable<VOMovingViolations>
{
	// Atributos
	
	/**
	 * Atributo que representa el identificador �nico de la infracci�n.
	 */
	private int objectID;
	
	/**
	 * Atributo que representa la direcci�n de la infracci�n. 
	 */
	private String location;
	
	/**
	 * Atributo que representa la fecha de la infracci�n. 
	 */
	private String issueDate;
	
	/**
	 * Atributo que representa el monto que se pag� efectivamente por la infracci�n.
	 */
	private int totalPaid;
	
	/**
	 * Atributo que representa si hubo un accidente o no. 
	 */
	private String accidentIndicator;
	
	/**
	 * Atributo que representa la decripci�n de la infracci�n. 
	 */
	private String violationDescription;
	
	/**
	 * Atributo que representa el c�digo de violaci�n. 
	 */
	private String violationCode;
	
	// Constructor
	
	/**
	 * Constructor del objeto de la violaci�n de transito. 
	 * @param pObjectID Identificador �nico de la infracci�n. 
	 * @param pLocation Direcci�n de la infracci�n. 
	 * @param pIssueDate Fecha de la infracci�n. 
	 * @param pTotalPaid Dinero efectivamente pagado por la infracci�n. pTotalPaid >= 0.
	 * @param pAccidentIndicator Indicador de si hubo accidente o no. 
	 * @param pViolationDescription Descipci�n de la infracci�n. 
	 */
	public VOMovingViolations(int pObjectID, String pLocation, String pCode, String pIssueDate, int pTotalPaid, String pAccidentIndicator, String pViolationDescription)
	{
		objectID = pObjectID;
		location = pLocation;
		violationCode = pCode;
		issueDate = pIssueDate;
		totalPaid = pTotalPaid;
		accidentIndicator = pAccidentIndicator;
		violationDescription = pViolationDescription;
	}
	
	// M�todos
	
	/**
	 * Retorna el c�digo de violaci�n.
	 * @return
	 */
	public String getCode()
	{
		return violationCode;
	}
	
	/**
	 * @return id - Identificador �nico de la infracci�n. 
	 */
	public int objectId() 
	{
		return objectID;
	}	
	
	
	/**
	 * @return location - Direcci�n en formato de texto.
	 */
	public String getLocation() 
	{
		return location;
	}

	/**
	 * @return date - Fecha cuando se puso la infracci�n.
	 */
	public String getTicketIssueDate() 
	{
		return issueDate;
	}
	
	/**
	 * @return totalPaid - Cuanto dinero efectivamente pag� el que recibi� la infracci�n en USD.
	 */
	public int getTotalPaid() 
	{
		return totalPaid;
	}
	
	/**
	 * @return accidentIndicator - Si hubo un accidente o no.
	 */
	public String  getAccidentIndicator() 
	{
		return accidentIndicator;
	}
		
	/**
	 * @return description - Descripci�n textual de la infracci�n.
	 */
	public String  getViolationDescription() 
	{
		return violationDescription;
	}

	@Override
	/**
	 * M�todo encargado de comparar por medio del identificador �nico. 
	 */
	public int compareTo(VOMovingViolations pVOMovingViolation) 
	{
		return objectID - pVOMovingViolation.objectId();
	}
}
