package model.data_structures;

/**
 * Abstract Data Type for a linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface ILinkedList<T extends Comparable<T>> extends Iterable<T> 
{
	/**
	 * Retorna el elemento pasado por par�metro si lo encuentra.
	 * @return el elemento si lo encuentra, null si no.
	 */
	public T search(T dato);
	
	/**
	 * Agrega el elemento como primero a la lista.
	 */
	public void add(T dato);
	
	/**
	 * Retorna el �ltimo elemento de la lista.
	 * @return el �ltimo elemento de la lista, null si no hay elementos.
	 */
	public T getLast();
	
	/**
	 * Retorna el primer elemento de la lista.
	 * @return el primer elemento de la lista, null si no hay elementos. 
	 */
	public T getFirst();
	
	/**
	 * Retorna el tama�o de la lista encadenada.
	 * @return el tama�o de la lista. 
	 */
	public Integer getSize();
}
