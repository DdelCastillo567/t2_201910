package controller;

import api.IMovingViolationsManager;
import model.data_structures.LinkedList;
import model.logic.MovingViolationsManager;
import model.vo.VOMovingViolations;

public class Controller 
{
	// Constante
	
	public static final String DIRECCION = "./data/Moving_Violations_Issued_in_January_2018.csv";
	
	// Atributos 
	/**
	 * Reference to the services manager
	 */
	@SuppressWarnings("unused")
	private static IMovingViolationsManager  manager = new MovingViolationsManager();
	
	// Constructor
	
	public static void loadMovingViolations() 
	{
		manager = new MovingViolationsManager();
		manager.loadMovingViolations(DIRECCION);
	}
	
	public static LinkedList <VOMovingViolations> getMovingViolationsByViolationCode (String violationCode) 
	{
		return manager.getMovingViolationsByViolationCode(violationCode);
	}
	
	public static LinkedList <VOMovingViolations> getMovingViolationsByAccident(String accidentIndicator) 
	{
		return manager.getMovingViolationsByAccident(accidentIndicator);
	}
}
