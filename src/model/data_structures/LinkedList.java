package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class LinkedList<T extends Comparable<T>> implements ILinkedList<T>
{
	// Atributos

	/**
	 * Atributo que representa el primer elemento de la lista. 
	 */
	private Node<T> primero;

	/**
	 * Atributo que representa el �ltimo elemento de la lista.
	 */
	private Node<T> ultimo;

	/**
	 * Atributo que representa el tama�o de la lista de elementos. 
	 */
	private int tamano;

	// Constructor

	/**
	 * Construye la lista enlazada de elementos.
	 */
	public LinkedList() 
	{
		primero = null;
		ultimo = null;
		tamano = 0;
	}

	// M�todos

	@Override
	/**
	 * Retorna el elemento pasado por par�metro si lo encuentra.
	 * @return el elemento si lo encuentra, null si no.
	 */
	public T search(T dato) 
	{
		Iterator<T> it = iterator();
		while(it.hasNext())
		{
			T act = it.next();
			if(act.equals(dato))
				return act;
		}
		return null;
	}

	/**
	 * Agrega el elemento como primero a la lista.
	 */
	public void add(T dato) 
	{
		Node<T> act = new Node<T>(dato);
		if(primero == null)
		{
			primero = act;
			ultimo = act;
		}
		else
		{
			Node<T> sig = primero;
			act.cambiarSiguiente(sig);
			primero = act;
		}
		tamano++;
	}

	@Override
	/**
	 * Retorna el �ltimo elemento de la lista.
	 * @return el �ltimo elemento de la lista, null si no hay elementos.
	 */
	public T getLast() 
	{
		if(ultimo != null)
			return ultimo.darElemento();
		else
			return null;
	}

	@Override
	/**
	 * Retorna el primer elemento de la lista.
	 * @return el primer elemento de la lista, null si no hay elementos. 
	 */
	public T getFirst() 
	{
		if(primero != null)
			return primero.darElemento();
		else
			return null;
	}

	@Override
	/**
	 * Retorna el tama�o de la lista encadenada.
	 * @return el tama�o de la lista. 
	 */
	public Integer getSize() 
	{
		return tamano;
	}

	@Override
	/**
	 * Retorna el iterador de los elementos de la lista. 
	 */
	public Iterator<T> iterator() 
	{
		return new IteratorLista<>(primero);
	}

	// IteratorLista

	class IteratorLista<T extends Comparable<T>> implements Iterator<T>
	{
		// Atributos

		/**
		 * Atributo que representa el pr�ximo nodo del elemento gen�rico.
		 */
		private Node<T> proximo;

		// Constructor

		/**
		 * Construye el iterador de la lista encadenada. 
		 * @param primero
		 */
		public IteratorLista(Node<T> primero)
		{
			proximo = primero;
		}

		// M�todos

		@Override
		/**
		 * Revisa si hay un pr�ximo nodo por visitar. 
		 */
		public boolean hasNext() 
		{
			return proximo != null;
		}

		@Override
		/**
		 * Retorna el pr�ximo elemento gen�rico y actualiza el pr�ximo.
		 */
		public T next() 
		{
			if(proximo == null)
				throw new NoSuchElementException("No hay pr�ximo.");
			T elem = proximo.darElemento();
			proximo = proximo.darSiguiente();
			return elem;
		}
	}
}
