package model.data_structures;

import static org.junit.Assert.*;

import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;

public class LinkedListTest 
{
	// Atributos
	
	private LinkedList list;
	
	private Node<String> node1;

	@Before
	public void setup()
	{
		list = new LinkedList();
		node1 = new Node<String>("Hola");
	}
	
	@Test
	public void testInicializacion()
	{
		assertTrue("El tama�o de la lista deber�a ser 0.", list.getSize()==0);
		assertTrue("El iterator no deber�a ser null.", list.iterator() != null);
		assertNull("El primero deber�a ser null.", list.getFirst());
		assertNull("El �ltimo deber�a ser null.", list.getLast());
	}
	
	@Test
	public void testAgregar()
	{
		list.add(node1);
		assertTrue("El tama�o de la lista deber�a ser 1.", list.getSize()==1);
		assertTrue("El primero no deber�a ser null.", list.getFirst() != null);
		assertTrue("El �ltimo no deber�a ser null.", list.getLast() != null);
	}
	
	@Test
	public void testFuncional()
	{
		list.add(node1);
		assertEquals("Deber�a encontrar el nodo.", node1, list.search(node1));
		Iterator it = list.iterator();
		assertTrue("El iterador deber�a tener siguiente.", it.hasNext());
		assertTrue("El iterador no deber�a devolver null.", it.next() != null);
	}
}
