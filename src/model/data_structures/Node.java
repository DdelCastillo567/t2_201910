package model.data_structures;

public class Node<T extends Comparable<T>> implements Comparable<T>
{
	// Atributos
	
	/**
	 * Elemento gen�rico del nodo.
	 */
	private T elemento;
	
	/**
	 * Siguiente elemento gen�rico del nodo. 
	 */
	private Node<T> siguiente;
	
	// Constructor
	
	/**
	 * Construye el nodo para el elemento gen�rico. siguiente = null. 
	 * @param pElemento elemento gen�rico a almacenar dentro del nodo. 
	 */
	public Node(T pElemento)
	{
		elemento = pElemento;
		siguiente = null;
		check();
	}
	
	// M�todos
	
	/**
	 * Retorna el elemento gen�rico asignado al nodo. 
	 * @return el elemento gen�rico.
	 */
	public T darElemento()
	{
		return elemento;
	}
	
	/**
	 * Retorna el siguiente elemento gen�rico asignado.
	 * @return el siguiente elemento gen�rico.
	 */
	public Node<T> darSiguiente()
	{
		return siguiente;
	}
	
	/**
	 * Cambia el siguiente nodo del elemento gen�rico por el dado por par�metro.
	 * @param pSiguiente nodo del elemento gen�rico a asignar como siguiente. 
	 */
	public void cambiarSiguiente(Node<T> pSiguiente)
	{
		siguiente = pSiguiente;
	}
	
	// Invariante
	
	/**
	 * Contrato que se asegura de que no se agregue un elemento null.
	 */
	private void check()
	{
		assert elemento != null : "El elemento no puede ser null.";
	}

	@Override
	public int compareTo(T pNode) 
	{
		if(this.equals(pNode))
			return 0;
		else
			return 1;
	}
}
